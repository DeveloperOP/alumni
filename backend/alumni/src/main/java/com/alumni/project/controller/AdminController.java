package com.alumni.project.controller;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alumni.project.model.User;
import com.alumni.project.model.UserList;
import com.alumni.project.service.GenericService;

@RestController
@RequestMapping(value = "admin")
public class AdminController {
	@Autowired
    private GenericService genericService;
	
	@RequestMapping(value="/allStudents", method=RequestMethod.GET)
	public Collection<UserList> getAllUsers() {
		return genericService.findAllStudents();
	}
	
	@RequestMapping(value="/users/{username}")
	public Optional<User> getUserByUsername(@PathVariable("username") String username) {
		return genericService.findByUsername(username);
	}
	
	@RequestMapping(value="/users/id/{id}")
	public Optional<User>getUserByUsername(@PathVariable("id") long id){
		return genericService.findById(id);
	}
	
	@RequestMapping(value="/allAlumni", method=RequestMethod.GET)
	public Collection<UserList> getAllAlumni() {
		return genericService.getAllAlumni();
	}
	
	@RequestMapping(value="/unapproved", method=RequestMethod.GET)
	public List<UserList> getUnapprovedUsers() {
		return genericService.getAllUnapproved();
	}
	
	@RequestMapping(value="/approve/{userID}", method=RequestMethod.GET)
	public String approveUser(@PathVariable("userID") long id) {
		return genericService.approveUser(id);
	}
	
}
