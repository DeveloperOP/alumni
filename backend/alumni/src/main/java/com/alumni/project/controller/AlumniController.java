package com.alumni.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alumni.project.dto.BlogDTO;
import com.alumni.project.dto.PostDTO;
import com.alumni.project.model.Blog;
import com.alumni.project.model.Post;
import com.alumni.project.service.GenericService;

@RestController
@RequestMapping(value="alumni")
public class AlumniController {
	@Autowired
    private GenericService genericService;
	
	@RequestMapping(value ="/addPost", method=RequestMethod.POST)
	public String addPost(@RequestBody PostDTO post) {
		Post newPost = new Post();
		newPost.setUserId(post.getUserID());
		newPost.setHead(post.getHeading());
		newPost.setBody(post.getDescription());
		return genericService.addPost(newPost);
	}
	
	@RequestMapping(value = "/addBlog", method=RequestMethod.POST)
	public String addBlog(@RequestBody BlogDTO blog) {
		Blog newBlog = new Blog();
		newBlog.setUserId(blog.getUserID());
		newBlog.setHead(blog.getHeading());
		newBlog.setBody(blog.getDescription());
		return genericService.addBlog(newBlog);
	}
}
