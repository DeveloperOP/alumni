CREATE TABLE `app_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `linkedin` varchar(255) DEFAULT NULL,
  `github` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `instagram` varchar(255) DEFAULT NULL,
  `user_profile` longblob DEFAULT NULL,
  `description` blob DEFAULT NULL,
  `role` varchar(25),
  `user_status` tinyint(1) NOT NULL DEFAULT '0',
  `timestamp` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `post` (
   	`postId`    bigint(20) NOT NULL AUTO_INCREMENT,
	`userId`    bigint(20) NOT NULL,
	`head`	    varchar(255) NOT NULL,
	`body`      varchar(1000),
	`timestamp` varchar(255) DEFAULT NULL,
	`image`	    varchar(500) DEFAULT NULL,
    `status`    boolean default true,
    PRIMARY KEY (`postId`)
) ENGINE=InnoDB;

CREATE TABLE `blog` (
   	`blogId`    bigint(20) NOT NULL AUTO_INCREMENT,
	`userId`    bigint(20) NOT NULL,
	`head`	    varchar(255) NOT NULL,
	`body`      varchar(1000),
	`timestamp` varchar(255) DEFAULT NULL,
    `status`    boolean DEFAULT true,
    PRIMARY KEY (`blogId`)
) ENGINE=InnoDB;




