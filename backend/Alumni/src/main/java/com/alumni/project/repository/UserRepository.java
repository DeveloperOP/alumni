package com.alumni.project.repository;

import java.util.Collection;



import java.util.List;
import java.util.Optional;

import com.alumni.project.dto.UserDTO;
import com.alumni.project.model.User;
import com.alumni.project.model.UserList;

public interface UserRepository {

	Optional<User> findByUsername(String username);
	Optional<User> saveUser(UserDTO user);
	Optional<User> findById(long id);
	Collection<UserList> findAllStudents();
	String approveUser(long id);
	List<UserList> getAllAlumni();
	List<UserList> getAllUnapproved();
	

}
