package com.alumni.project.service.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;


import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.alumni.project.dto.UserDTO;
import com.alumni.project.model.Blog;
import com.alumni.project.model.Post;
import com.alumni.project.model.User;
import com.alumni.project.model.UserList;
import com.alumni.project.repository.BlogRepository;
import com.alumni.project.repository.PostRepository;
import com.alumni.project.repository.UserRepository;
import com.alumni.project.service.GenericService;

@Service
public class GenericServiceImpl implements GenericService {
	
	@Resource 
    private UserRepository userRepository;
	@Resource 
    private PostRepository postRepository;
	@Resource
	private BlogRepository blogRepository;
	
	
	@Override
	public Optional<User> findByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	@Override
	public List<User> findAllUsers() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<User> saveUser(UserDTO user) {
			return userRepository.saveUser(user);
	}

	@Override
	public Optional<User> findById(long id) {
		return userRepository.findById(id);
	}

	@Override
	public Collection<UserList> findAllStudents() {
		return userRepository.findAllStudents();
	}

	@Override
	public String approveUser(long id) {
		return userRepository.approveUser(id);
		
	}

	@Override
	public Collection<UserList> getAllAlumni() {
		return userRepository.getAllAlumni();
	}

	@Override
	public List<UserList> getAllUnapproved() {
		return userRepository.getAllUnapproved();
	}

	@Override
	public String addPost(Post post) {
		return postRepository.addPost(post);
	}

	@Override
	public String addBlog(Blog blog) {
		return blogRepository.addBlog(blog);
	}

	@Override
	public List<Map<String, Object>> showAllPost() {
		return postRepository.showAllPost();
	}
	
	@Override
	public List<Map<String, Object>> showAllBlog() {
		return blogRepository.showAllBlog();
	}

	@Override
	public Optional<Blog> getBlogById(long id) {
		return blogRepository.getBlogById(id);
	}

	@Override
	public List<Map<String, Object>> getBlogByUserId(long id) {
		return blogRepository.getBlogByUserId(id);
	}

	@Override
	public Optional<Post> getPostById(long id) {
		return postRepository.getPostById(id);
	}

	@Override
	public List<Map<String, Object>> getPostByUserId(long id) {
		return postRepository.getPostByUserId(id);
	}

	@Override
	public String deletePostById(long id) {
		return postRepository.deletePostById(id);
	}

	@Override
	public String deleteBlogById(long id) {
		return blogRepository.deleteBlogById(id);
	}

	@Override
	public void saveImage(MultipartFile imageFile) throws Exception {
		String folder = "/Users/atharvdeshmukh/storage/photos/";	//TODO: Change this location to your wanted dest.
		byte[] bytes = imageFile.getBytes();
		Path path = Paths.get(folder+imageFile.getOriginalFilename());
		Files.write(path, bytes);
	}
	
	
}
