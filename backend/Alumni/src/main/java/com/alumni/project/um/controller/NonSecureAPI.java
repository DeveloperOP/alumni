package com.alumni.project.um.controller;

import java.util.Optional;

import javax.annotation.Resource;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alumni.project.dto.UserDTO;
import com.alumni.project.service.GenericService;
import com.alumni.project.model.User;
import com.alumni.project.util.bCryptPasswordUtility;
@RequestMapping(value = "userMgt")
@RestController
public class NonSecureAPI {
	@Autowired
    private GenericService genericService;
	@Resource(name="bCryptPasswordUtility")
	private bCryptPasswordUtility passwordUtil; 
	
	
	@RequestMapping(value="/addUser", method=RequestMethod.POST)
	public Optional<User>  addUser(@RequestBody UserDTO user) {
		Optional<User> existingUser = Optional.of(new User());
		existingUser =  genericService.findByUsername(user.getUserName());
		if(existingUser.isPresent()) {
			return existingUser;
			// TODO: Add server response
		}
		else {
				return genericService.saveUser(user);
		}
	}
}
