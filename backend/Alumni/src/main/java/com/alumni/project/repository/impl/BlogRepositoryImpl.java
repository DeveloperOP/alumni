package com.alumni.project.repository.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;
import javax.sql.DataSource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import com.alumni.project.model.Blog;
import com.alumni.project.model.Post;
import com.alumni.project.model.User;
import com.alumni.project.repository.BlogRepository;


@PropertySource(value = { "classpath:/queries/queries.properties" })
@Repository
public class BlogRepositoryImpl extends JdbcDaoSupport implements BlogRepository {

	@Resource 
	private Environment environment;
	
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	public BlogRepositoryImpl(DataSource dataSource) {
		super.setDataSource(dataSource);
		jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	@Override
	public String addBlog(Blog blog) {
		String query = environment.getProperty("SAVE_BLOG");
		SqlParameterSource paramSource = new MapSqlParameterSource()
				.addValue("userId", blog.getUserId())
				.addValue("head", blog.getHead())
				.addValue("body", blog.getBody().toString());
		KeyHolder keyHolder = new GeneratedKeyHolder();
		int i = jdbcTemplate.update(query, paramSource, keyHolder);
		if(i>0) {
			return "Blog added";
		}
		else
			return null;
	}

	@Override
	public List<Map<String, Object>> showAllBlog() {
		String query = environment.getProperty("FIND_ALL_BLOG");
		try {
			List<Map<String, Object>> rows = getJdbcTemplate().queryForList(query);
			
			if(rows!=null) {
				return rows;
			}
		} catch (EmptyResultDataAccessException exception) {
			return null;
		}
		return null;
	}

	@Override
	public Optional<Blog> getBlogById(long id) {
		String query = environment.getProperty("FIND_BLOG_BY_ID");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("blogId", id);
		try {
			Blog blog = jdbcTemplate.queryForObject(query, paramMap,new BeanPropertyRowMapper<Blog>(Blog.class));
			if(blog!=null) {
				return Optional.of(blog);
			}
			return Optional.ofNullable(blog);
		} catch (EmptyResultDataAccessException exception) {
			return Optional.empty();
		}
	}

	@Override
	public List<Map<String, Object>> getBlogByUserId(long id) {
		String query = "SELECT * FROM alumni.blog WHERE userId="+ id +" AND status=true;";
		try {
			List<Map<String, Object>> rows = getJdbcTemplate().queryForList(query);			
			if(rows!=null) {
				return rows;
			}
		} catch (EmptyResultDataAccessException exception) {
			return null;
		}
		return null;
	}

	@Override
	public String deleteBlogById(long id) {
		String query = environment.getProperty("DELETE_BLOG");
		SqlParameterSource paramSource = new MapSqlParameterSource()
				.addValue("id", id);
		KeyHolder keyHolder = new GeneratedKeyHolder();
		int i = jdbcTemplate.update(query, paramSource, keyHolder);
		if(i>0) {
			return "Blog deleted";
		}
		else
			return null;
	}

}
