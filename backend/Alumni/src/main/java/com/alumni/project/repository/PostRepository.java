package com.alumni.project.repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Repository;
import com.alumni.project.model.Post;

@Repository
public interface PostRepository {
	String addPost(Post post);
	List<Map<String, Object>> showAllPost();
	Optional<Post> getPostById(long id);
	List<Map<String, Object>> getPostByUserId(long id);
	String deletePostById(long id);
}
