package com.alumni.project.util;

import org.springframework.stereotype.Component;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Component("bCryptPasswordUtility")
public class bCryptPasswordUtility {
	private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
	public String encryptPassword(String password) {
		return encoder.encode(password);
	}
	public Boolean match(String encodedPassword, String newSource) {
		return encoder.matches(newSource, encodedPassword);
	}
	public static void main1(String[] args) {
		System.out.println(new BCryptPasswordEncoder().encode("admin12356"));
	}
}

