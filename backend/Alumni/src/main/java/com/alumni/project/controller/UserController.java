package com.alumni.project.controller;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.alumni.project.model.Blog;
import com.alumni.project.model.Post;
import com.alumni.project.service.GenericService;

@RequestMapping(value="all")
@RestController
public class UserController {
	
	@Resource
    private GenericService userService;
	
	@RequestMapping(value="allPost", method=RequestMethod.GET)
	public List<Map<String, Object>> allPost() {
		return userService.showAllPost();
	}
	
	@RequestMapping(value="allBlog", method=RequestMethod.GET)
	public List<Map<String, Object>> allBlog() {
		return userService.showAllBlog();
	}
	
	@RequestMapping(value="blog/{id}", method=RequestMethod.GET)
	public Optional<Blog> getBlogById(@PathVariable("id") long id) {
		return userService.getBlogById(id);
	}
	
	@RequestMapping(value="post/{id}", method=RequestMethod.GET)
	public Optional<Post> getPostById(@PathVariable("id") long id) {
		return userService.getPostById(id);
	}
	
	@RequestMapping(value="blog/userID/{id}", method=RequestMethod.GET)
	public List<Map<String, Object>> getBlogByUserId(@PathVariable("id") long id) {
		return userService.getBlogByUserId(id);
	}
	
	@RequestMapping(value="post/userID/{id}", method=RequestMethod.GET)
	public List<Map<String, Object>> getPostByUserId(@PathVariable("id") long id) {
		return userService.getPostByUserId(id);
	}
	
	@RequestMapping(value="post/delete/{id}", method=RequestMethod.GET)
	public String deletePostById(@PathVariable("id") long id) {
		return userService.deletePostById(id);
	}
	
	@RequestMapping(value="blog/delete/{id}", method=RequestMethod.GET)
	public String deleteBlogById(@PathVariable("id") long id) {
		return userService.deleteBlogById(id);
	}
	
	@RequestMapping(value="uploadImage", method=RequestMethod.POST)
	public String uploadImage(@RequestParam("imageFile") MultipartFile imageFile) {
		String returnValue;
		try {
			userService.saveImage(imageFile);
			returnValue="Image! Uploaded";
		}
		catch(Exception e) {
			e.printStackTrace();
			returnValue="Error While Uploading Image!";
		}
		
		return returnValue;
	}
	
}
