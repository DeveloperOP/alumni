package com.alumni.project.service;

import java.util.Collection;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

import com.alumni.project.dto.UserDTO;
import com.alumni.project.model.Blog;
import com.alumni.project.model.Post;
import com.alumni.project.model.User;
import com.alumni.project.model.UserList;

public interface GenericService {
	
	// User Related
    Optional<User> findByUsername(String username);
    Optional<User> findById(long id);
    List<User> findAllUsers();
    Collection<UserList> findAllStudents();
    Collection<UserList> getAllAlumni();
    Optional<User> saveUser(UserDTO user);
    String approveUser(long id);
    List<UserList> getAllUnapproved();
    
    // Post Related
    String addPost(Post post);
	List<Map<String, Object>> showAllPost();
	Optional<Post> getPostById(long id);
	List<Map<String, Object>> getPostByUserId(long id);

    //Blog Related
    String addBlog(Blog blog);
    List<Map<String, Object>> showAllBlog();
	Optional<Blog> getBlogById(long id);
	List<Map<String, Object>> getBlogByUserId(long id);
	String deletePostById(long id);
	String deleteBlogById(long id);
	
	// Image
	void saveImage(MultipartFile imageFile) throws Exception;
	
}
