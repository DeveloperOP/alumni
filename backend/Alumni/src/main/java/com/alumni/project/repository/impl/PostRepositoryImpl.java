package com.alumni.project.repository.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;


import javax.sql.DataSource;

import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.alumni.project.model.Post;
import com.alumni.project.model.UserList;
import com.alumni.project.repository.PostRepository;

@PropertySource(value = { "classpath:/queries/queries.properties" })
@Repository
public class PostRepositoryImpl extends JdbcDaoSupport implements PostRepository{
	
	@Resource 
	private Environment environment;
	
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	public PostRepositoryImpl(DataSource dataSource) {
		super.setDataSource(dataSource);
		jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	@Override
	public String addPost(Post post) {
		String query = environment.getProperty("SAVE_POST");
		SqlParameterSource paramSource = new MapSqlParameterSource()
				.addValue("userId", post.getUserId())
				.addValue("head", post.getHead())
				.addValue("body", post.getBody().toString());
		KeyHolder keyHolder = new GeneratedKeyHolder();
		int i = jdbcTemplate.update(query, paramSource, keyHolder);
		if(i>0) {
			return "Post added";
		}
		else
			return null;
	}

	@Override
	public List<Map<String,Object>> showAllPost() {
		String query = environment.getProperty("FIND_ALL_POST");
		try {
			List<Map<String, Object>> rows = getJdbcTemplate().queryForList(query);
			
			if(rows!=null) {
				return rows;
			}
		} catch (EmptyResultDataAccessException exception) {
			return null;
		}
		return null;
	}
	
	@Override
	public Optional<Post> getPostById(long id) {
		String query = environment.getProperty("FIND_POST_BY_ID");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("postId", id);
		try {
			Post post = jdbcTemplate.queryForObject(query, paramMap,new BeanPropertyRowMapper<Post>(Post.class));
			if(post!=null) {
				return Optional.of(post);
			}
			return Optional.ofNullable(post);
		} catch (EmptyResultDataAccessException exception) {
			return Optional.empty();
		}
	}

	@Override
	public List<Map<String, Object>> getPostByUserId(long id) {
		String query = "SELECT * FROM alumni.post WHERE userId="+ id +" AND status=true;";
		try {
			List<Map<String, Object>> rows = getJdbcTemplate().queryForList(query);			
			if(rows!=null) {
				return rows;
			}
		} catch (EmptyResultDataAccessException exception) {
			return null;
		}
		return null;
	}

	@Override
	public String deletePostById(long id) {
		String query = environment.getProperty("DELETE_POST");
		SqlParameterSource paramSource = new MapSqlParameterSource()
				.addValue("id", id);
		KeyHolder keyHolder = new GeneratedKeyHolder();
		int i = jdbcTemplate.update(query, paramSource, keyHolder);
		if(i>0) {
			return "Post deleted";
		}
		else
			return null;
	}
}
