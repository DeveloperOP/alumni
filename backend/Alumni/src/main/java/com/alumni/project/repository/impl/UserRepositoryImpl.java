package com.alumni.project.repository.impl;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.annotation.Resource;

import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.alumni.project.dto.UserDTO;
import com.alumni.project.model.User;
import com.alumni.project.model.UserList;
import com.alumni.project.repository.UserRepository;
import com.alumni.project.util.bCryptPasswordUtility;

@PropertySource(value = { "classpath:/queries/queries.properties" })
@Repository
public class UserRepositoryImpl extends JdbcDaoSupport implements UserRepository  {
	@Resource 
	private Environment environment;
	
	@Resource(name="bCryptPasswordUtility")
	private bCryptPasswordUtility passwordUtility;
	
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	public UserRepositoryImpl(javax.sql.DataSource dataSource) {
		super.setDataSource(dataSource);
		jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}
	
	@Override
	public Optional<User> findByUsername(String username) {
		String query = environment.getProperty("FIND_USER_BY_USERNAME");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("username", username);
		try {
			User user = jdbcTemplate.queryForObject(query, paramMap,new BeanPropertyRowMapper<User>(User.class));
			if(user!=null) {
				return Optional.of(user);
			}
			return Optional.ofNullable(user);
		} catch (EmptyResultDataAccessException exception) {
			return Optional.empty();
		}
	}

	@Override
	public Optional<User> saveUser(UserDTO user) {
		String query = environment.getProperty("SAVE_USER");
		SqlParameterSource paramSource = new MapSqlParameterSource()
				.addValue("first_name", user.getFirstName())
				.addValue("last_name", user.getLastName())
				.addValue("password", passwordUtility.encryptPassword(user.getPassword()))
				.addValue("username", user.getUserName())
				.addValue("role", user.getRole());
		KeyHolder keyHolder = new GeneratedKeyHolder();
		int i = jdbcTemplate.update(query, paramSource, keyHolder);
		if(i>0) {
			long id = keyHolder.getKey().longValue();
			return findById(id);
		}
		else {
			return null;
		}
	}

	@Override
	public Optional<User> findById(long id) {
		String query = environment.getProperty("FIND_BY_ID");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("id", id);
		try {
			User user = jdbcTemplate.queryForObject(query, paramMap,new BeanPropertyRowMapper<User>(User.class));
			if(user!=null) {
				return Optional.of(user);
			}
			return Optional.ofNullable(user);
		} catch (EmptyResultDataAccessException exception) {
			return Optional.empty();
		}
	}

	@Override
	public List<UserList> findAllStudents() {
		String query = environment.getProperty("FIND_ALL_STUDENTS");
		try {
			List<UserList> user = new ArrayList<>();

			List<Map<String, Object>> rows = getJdbcTemplate().queryForList(query);
			
			for(Map<String, Object> row:rows){
				UserList student = new UserList();
				student.setId((long)row.get("id"));
				student.setFirst_name((String)row.get("first_name"));
				student.setLast_name((String)row.get("last_name"));
				student.setUsername((String)row.get("username"));
				user.add(student);
			}
			
			if(user!=null) {
				return user;
			}
		} catch (EmptyResultDataAccessException exception) {
			return null;
		}
		return null;
	}

	@Override
	public String approveUser(long id) {
		String query = environment.getProperty("APPROVE_BY_ID");
		SqlParameterSource paramSource = new MapSqlParameterSource()
				.addValue("id", id);
		KeyHolder keyHolder = new GeneratedKeyHolder();
		try {
			int i = jdbcTemplate.update(query, paramSource, keyHolder);
			if(i>0) {
				return "User Approved!";
			}
			else {
				return null;
			}
		}
		catch (EmptyResultDataAccessException exception) {
				return "Error";
		}
		
	}

	@Override
	public List<UserList> getAllAlumni() {
		String query = environment.getProperty("FIND_ALL_ALUMNI");
		try {
			List<UserList> user = new ArrayList<>();

			List<Map<String, Object>> rows = getJdbcTemplate().queryForList(query);
			
			for(Map<String, Object> row:rows){
				UserList alumni = new UserList();
				alumni.setId((long)row.get("id"));
				alumni.setFirst_name((String)row.get("first_name"));
				alumni.setLast_name((String)row.get("last_name"));
				alumni.setUsername((String)row.get("username"));
				user.add(alumni);
			}
			if(user!=null) {
				return user;
			}
		} catch (EmptyResultDataAccessException exception) {
			return null;
		}
		return null;
		
	}

	@Override
	public List<UserList> getAllUnapproved() {
		String query = environment.getProperty("FIND_ALL_UNAPPROVED");
		try {
			List<UserList> user = new ArrayList<>();

			List<Map<String, Object>> rows = getJdbcTemplate().queryForList(query);
			for(Map<String, Object> row:rows){
				UserList users = new UserList();
				users.setId((long)row.get("id"));
				users.setFirst_name((String)row.get("first_name"));
				users.setLast_name((String)row.get("last_name"));
				users.setUsername((String)row.get("username"));
				user.add(users);
			}
			if(user!=null) {
				return user;
			}
		} catch (EmptyResultDataAccessException exception) {
			return null;
		}
		return null;
	}

}
