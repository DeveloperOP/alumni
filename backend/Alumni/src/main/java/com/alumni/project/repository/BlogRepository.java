package com.alumni.project.repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Repository;
import com.alumni.project.model.Blog;
import com.alumni.project.model.Post;

@Repository
public interface BlogRepository {
		String addBlog(Blog blog);

		List<Map<String, Object>> showAllBlog();
		Optional<Blog> getBlogById(long id);
		List<Map<String, Object>> getBlogByUserId(long id);

		String deleteBlogById(long id);
}
